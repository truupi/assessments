﻿using ElectoolAssessments;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssessmentUnitTests
{
    [TestFixture]
    public class GigaSecondUnitTests
    {
        [Test]
        public void CalculateGigaSecond_ReturnsProperDate()
        {
            var date = GigaSecondCalculator.CalculateGigaSecond(new DateTime(1910, 3, 3, 0, 0, 0, DateTimeKind.Utc));
            Assert.That(date, Is.EqualTo(new DateTime(1941, 11, 9, 1, 46, 40, DateTimeKind.Utc)));
        }

        [Test]
        public void CalculateGigaSecond_ReturnsProperDate2()
        {
            var date = GigaSecondCalculator.CalculateGigaSecond(new DateTime(1993, 1, 24, 1, 55, 0, DateTimeKind.Utc));
            Assert.That(date, Is.EqualTo(new DateTime(2024, 10, 2, 3, 41, 40, DateTimeKind.Utc)));
        }
    }
}
