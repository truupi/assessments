using ElectoolAssessments;
using NUnit.Framework;

namespace AssessmentUnitTests
{
    [TestFixture]
    public class LeapYearUnitTests
    {
        [TestCase(1616)]
        [TestCase(2008)]
        [TestCase(2016)]
        public void IsLeapYear_YearIsLeapYear_ReturnsTrue(int year)
        {
            Assert.IsTrue(LeapYearChecker.IsLeapYear(year));
        }

        [TestCase(1200)]
        [TestCase(2000)]
        public void IsLeapYear_YearIsDividableBy400_ReturnsTrue(int year)
        {
            Assert.IsTrue(LeapYearChecker.IsLeapYear(year));
        }

        [TestCase(300)]
        [TestCase(1100)]
        public void IsLeapYear_YearIsDividableBy100Only_ReturnsFalse(int year)
        {
            Assert.IsFalse(LeapYearChecker.IsLeapYear(year));
        }

        [TestCase(1617)]
        [TestCase(2007)]
        public void IsLeapYear_YearIsNotLeapYear_ReturnsFalse(int year)
        {
            Assert.IsFalse(LeapYearChecker.IsLeapYear(year));
        }
    }
}
