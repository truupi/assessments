﻿using ElectoolAssessments;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssessmentUnitTests
{
    [TestFixture]
    public class PascalUnitTests
    {
        [Test]
        public void TriangleDrawer_Input5_MatrixFilled()
        {
            var triangleToTest = PascalTriangleDrawer.DrawTriangle(5);

            Assert.AreEqual(3, triangleToTest[3, 1]);
            Assert.AreEqual(1, triangleToTest[1, 0]);
        }
    }
}
