﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElectoolAssessments
{
    public class LeapYearChecker
    {
        public static bool IsLeapYear(int year)
        {
            if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
            {
                return true;
            }
            return false;
        }
    }
}
