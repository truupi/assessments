﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElectoolAssessments
{
    public class PascalTriangleDrawer
    {
        public static int[,] DrawTriangle(int size)
        {
            int[,] triangle = new int[size, size];
            int row, col;

            for (row = 0; row < size; row++)
            {
                for (col = 0; col < size; col++)
                {
                    triangle[row, col] = 0;
                }
            }

            triangle[0, 0] = 1;
            triangle[1, 0] = 1;
            triangle[1, 1] = 1;

            for (row = 2; row < size; row++)
            {
                triangle[row, 0] = 1;
                for (col = 1; col <= row; col++)
                {
                    triangle[row, col] = triangle[row - 1, col - 1] + triangle[row - 1, col];
                }
            }

            return triangle;
        }
    }
}