﻿using System;

namespace ElectoolAssessments
{
    public class GigaSecondCalculator
    {
        private static TimeSpan GigaSecond = new TimeSpan(0, 0, 1000000000);

        public static DateTime CalculateGigaSecond(DateTime inputDate)
        {
            return inputDate.Add(GigaSecond);
        }
    }
}
